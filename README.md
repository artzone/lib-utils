[![Codacy Badge](https://app.codacy.com/project/badge/Grade/065356cb7e7447519664f8103a7cd37b)](https://www.codacy.com/gl/artzone/lib-utils/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=artzone/lib-utils&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/065356cb7e7447519664f8103a7cd37b)](https://www.codacy.com/gl/artzone/lib-utils/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=artzone/lib-utils&utm_campaign=Badge_Coverage)
# lib-utils

## Update project

### Checking for new plugin updates
`./mvnw --settings .m2/settings.xml versions:display-plugin-updates`

### Checking  for updated dependencies in repository
`./mvnw --settings .m2/settings.xml versions:display-dependency-updates`

### Checking  for style validation
`./mvnw  --settings .m2/settings.xml checkstyle:check`

### Checking  for bugs
`./mvnw  --settings .m2/settings.xml spotbugs:check`