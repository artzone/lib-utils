package pl.arturkb.artzone.utils;

import com.google.common.base.Strings;
import java.util.Objects;

/**
 * A class that offers different kind validations.
 */
public abstract class Validators {

  /**
   * Validate that the given parameter is not Null. If the given parameter is Null then
   * IllegalArgumentException is thrown. The exception message will include paramName in the body.
   *
   * @param object    the object to be validated
   * @param paramName the parameter name that wil be used in the message body
   */
  public static void validateNotNull(Object object, String paramName) {
    if (Objects.isNull(object)) {
      throw new IllegalArgumentException(String.format("%s can't be null", paramName));
    }
  }

  /**
   * Validate that the given parameter is not Null or empty string. If the given parameter is Null
   * then IllegalArgumentException is thrown. The exception message will include paramName in the
   * body.
   *
   * @param string    the String to be validated that is empty or Null
   * @param paramName the parameter name that wil be used in the message body
   */
  public static void validateNotNullAndNotEmpty(String string, String paramName) {
    if (Strings.isNullOrEmpty(string)) {
      throw new IllegalArgumentException(String.format("%s can't be null or empty", paramName));
    }
  }

  /**
   * Validate that the given parameter is not less than the given min. Throws
   * IllegalArgumentException if is not valid.
   *
   * @param param the parameter to check that is not less than given min
   * @param min   the min to check against
   */
  public static void validateNotLessThanMin(int param, int min) {
    if (param < min) {
      throw new IllegalArgumentException(String.format("%d can't be less than %d", param, min));
    }
  }

  /**
   * Validate that the given parameter is not greater than the given max. Throws
   * IllegalArgumentException if is not valid.
   *
   * @param param the parameter to check that is not greater than given max
   * @param max   the max to check against
   */
  public static void validateNotGreaterThanMax(int param, int max) {
    if (param > max) {
      throw new IllegalArgumentException(String.format("%d can't be greater than %d", param, max));
    }
  }

  /**
   * Validate that the given parameter is greater than the given min and less than the given max.
   * Throws IllegalArgumentException if is not valid.
   *
   * @param min   the minimum to check that parameter is greater
   * @param max   the maximum to check that parameter is less
   * @param param the parameter to check
   */
  public static void validateGreaterThanMinAndLessThanMax(int min, int max, int param) {
    validateMinLessThanMax(min, max);
    validateNotLessThanMin(param, min);
    validateNotGreaterThanMax(param, max);
  }

  /**
   * Validate that the given minimum is less than the given maximum. Throws IllegalArgumentException
   * if is not valid.
   *
   * @param min the minimum to check
   * @param max the maximum to check
   */
  public static void validateMinLessThanMax(final int min, final int max) {
    if (min > max) {
      throw new IllegalArgumentException("min must be greater then max");
    }
  }
}
