package pl.arturkb.artzone.utils;

@FunctionalInterface
public interface Validate<T> {

  boolean isValid(T object);
}
