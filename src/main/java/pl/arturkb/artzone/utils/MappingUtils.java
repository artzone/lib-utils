package pl.arturkb.artzone.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class MappingUtils {

  private static final String OBJECT_NOT_NULL = "The object parameter";
  private static final String LIST_NOT_NULL = "The list parameter";
  private static final String STRING_NOT_NULL_OR_EMPTY = "The string parameter";
  private static final String KLASS_NOT_NULL = "The klass parameter";
  private static final String READER_NOT_NULL = "The reader parameter";
  private static final String INPUTSTREAM_NOT_NULL = "The inputStream parameter";

  /**
   * Serialize object of T type as JSON objects. Throws JsonProcessingException if serialization to
   * JSON fails.
   *
   * @param object the object of type T to serialize as JSON objects
   * @param <T>    the type of object
   * @return the JSON
   * @throws JsonProcessingException the exception is thrown when serialization to JSON fails
   */
  public static <T> String getJson(@NotNull T object) throws JsonProcessingException {
    Validators.validateNotNull(object, OBJECT_NOT_NULL);
    return JacksonObjectMapperUtils.OBJECT_MAPPER.writeValueAsString(object);
  }

  /**
   * Serialize List of objects of T type as JSON objects. Throws JsonProcessingException if
   * serialization to JSON fails.
   *
   * @param objects the List of objects of type T to serialize as JSON objects
   * @param <T>     the type of object
   * @return the List of JSON
   * @throws JsonProcessingException the exception is thrown when serialization to JSON fails
   */
  public static <T> List<String> getJsons(@NotNull List<T> objects) throws JsonProcessingException {
    Validators.validateNotNull(objects, LIST_NOT_NULL);
    List<String> result = new ArrayList<>();

    for (T object : objects) {
      result.add(getJson(object));
    }

    return result;
  }

  /**
   * Deserialize an object form the given JSON by using the giving class type.
   *
   * @param json  the JSON to deserialize
   * @param klass the class to be used for deserialization
   * @param <T>   the type of the class
   * @return object of type T
   * @throws JsonProcessingException the exception is thrown when deserialization to JSON fails
   */
  public static <T> T getObject(String json, Class<T> klass) throws JsonProcessingException {
    Validators.validateNotNullAndNotEmpty(json, STRING_NOT_NULL_OR_EMPTY);
    Validators.validateNotNull(klass, KLASS_NOT_NULL);
    return JacksonObjectMapperUtils.OBJECT_MAPPER.readValue(json, klass);
  }

  /**
   * Deserialize an object form the given JSON by using the giving class type.
   *
   * @param json  the reader with the JSON to deserialize
   * @param klass the class to be used for deserialization
   * @param <T>   the type of the class
   * @return object of type T
   * @throws JsonProcessingException the exception is thrown when deserialization to JSON fails
   */
  public static <T> T getObject(Reader json, Class<T> klass) throws IOException {
    Validators.validateNotNull(json, READER_NOT_NULL);
    Validators.validateNotNull(klass, KLASS_NOT_NULL);
    return JacksonObjectMapperUtils.OBJECT_MAPPER.readValue(json, klass);
  }

  /**
   * Deserialize an object form the given JSON by using the giving class type.
   *
   * @param stream the inputStream with the JSON to deserialize
   * @param klass  the class to be used for deserialization
   * @param <T>    the type of the class
   * @return object of type T
   * @throws JsonProcessingException the exception is thrown when deserialization to JSON fails
   */
  public static <T> T getObject(InputStream stream, Class<T> klass) throws IOException {
    Validators.validateNotNull(stream, INPUTSTREAM_NOT_NULL);
    Validators.validateNotNull(klass, KLASS_NOT_NULL);
    return JacksonObjectMapperUtils.OBJECT_MAPPER.readValue(stream, klass);
  }

  /**
   * Deserialize an objects form the given JSON by using the giving class type.
   *
   * @param json  the JSON to deserialize
   * @param klass the class to be used for deserialization
   * @param <T>   the type of the class
   * @return object List of type T
   * @throws JsonProcessingException the exception is thrown when deserialization to JSON fails
   */
  public static <T> List<T> getObjects(String json, Class<T> klass)
      throws JsonProcessingException {
    Validators.validateNotNullAndNotEmpty(json, STRING_NOT_NULL_OR_EMPTY);
    Validators.validateNotNull(klass, KLASS_NOT_NULL);
    TypeFactory t = TypeFactory.defaultInstance();

    CollectionType valueType = t.constructCollectionType(ArrayList.class, klass);
    return JacksonObjectMapperUtils.OBJECT_MAPPER.readValue(json, valueType);
  }

  /**
   * Deserialize an objects form the given JSON by using the giving class type.
   *
   * @param jsons the List of JSON's to deserialize
   * @param klass the class to be used for deserialization
   * @param <T>   the type of the class
   * @return object List of type T
   * @throws JsonProcessingException the exception is thrown when deserialization to JSON fails
   */
  public static <T> List<T> getObjects(List<String> jsons, Class<T> klass)
      throws JsonProcessingException {
    Validators.validateNotNull(jsons, LIST_NOT_NULL);
    Validators.validateNotNull(klass, KLASS_NOT_NULL);
    List<T> result = new ArrayList<>();

    for (String json : jsons) {
      result.add(getObject(json, klass));
    }

    return result;
  }
}
