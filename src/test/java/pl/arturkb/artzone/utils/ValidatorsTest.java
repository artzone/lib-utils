package pl.arturkb.artzone.utils;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ValidatorsTest {

  @Test
  public void validateNotNullNotNull() {
    Object objectUnderTest = new Object();
    Validators.validateNotNull(objectUnderTest, objectUnderTest.getClass().getName());
    assertThat(true).isTrue();
  }

  @Test
  public void validateNotNullNull() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateNotNull(null, "Test"));
  }

  @Test
  public void validateNotNullAndNotEmptyNotNull() {
    String stringUnderTest = "notNullAndNotEmpty";
    Validators.validateNotNullAndNotEmpty(stringUnderTest, "Test");
    assertThat(true).isTrue();
  }

  @Test
  public void validateNotNullAndNotEmptyNull() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateNotNullAndNotEmpty(null, "Test"));
  }

  @Test
  public void validateNotNullAndNotEmptyEmpty() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateNotNullAndNotEmpty("", "Test"));
  }

  @Test
  public void validateNotLessThanMin() {
    Validators.validateNotLessThanMin(2, 0);
    assertThat(true).isTrue();
  }

  @Test
  public void validateNotLessThanMinLess() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateNotLessThanMin(-2, 0));
  }

  @Test
  public void validateNotGreaterThanMax() {
    Validators.validateNotGreaterThanMax(-2, 0);
    assertThat(true).isTrue();
  }

  @Test
  public void validateNotGreaterThanMaxGreater() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateNotGreaterThanMax(2, 0));
  }

  @Test
  public void validateGreaterThanMinAndLessThanMax() {
    Validators.validateGreaterThanMinAndLessThanMax(0, 2, 1);
    assertThat(true).isTrue();
  }

  @Test
  public void validateGreaterThanMinAndLessThanMaxLessThanMin() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateGreaterThanMinAndLessThanMax(0, 2, -1));
  }

  @Test
  public void validateGreaterThanMinAndLessThanMaxGreaterThanMax() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateGreaterThanMinAndLessThanMax(0, 2, 3));
  }

  @Test
  public void validateMinLessThanMax() {
    Validators.validateMinLessThanMax(1, 2);
    assertThat(true).isTrue();
  }

  @Test
  public void validateMinNotLessThanMax() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> Validators.validateMinLessThanMax(3, 2));
  }

  @Test
  public void validateMinEqualsLessThanMax() {
    Validators.validateMinLessThanMax(2, 2);
    assertThat(true).isTrue();
  }
}
