package pl.arturkb.artzone.utils;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Objects;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class MappingUtilsTest {

  private static final String ARTUR_JSON = "{\"firstName\":\"Artur\",\"lastName\":\"Barczynski\"}";
  private static final String KAROLINA_JSON =
      "{\"firstName\":\"Karolina\",\"lastName\":\"Barczynski\"}";
  private static final String ARTUR_KAROLINA_JSON =
      "[{\"firstName\":\"Karolina\",\"lastName\":"
          + "\"Barczynski\"},{\"firstName\":\"Artur\",\"lastName\":\"Barczynski\"}]";

  private TestKlass artur = new TestKlass("Artur", "Barczynski");
  private TestKlass karolina = new TestKlass("Karolina", "Barczynski");

  @Test
  public void getJsonHappyDay() throws JsonProcessingException, JSONException {
    final String json = MappingUtils.getJson(artur);
    assertThat(json).isNotEmpty();
    JSONAssert.assertEquals(ARTUR_JSON, json, true);
  }

  @Test
  public void getJsonNull() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils.getJson(null));
  }

  @Test
  public void getJsonsHappyDay() throws JsonProcessingException {
    final List<String> jsons = MappingUtils.getJsons(Lists.asList(artur, karolina, new String[0]));
    assertThat(jsons).isNotEmpty();
    assertThat(jsons).contains(ARTUR_JSON);
    assertThat(jsons).contains(KAROLINA_JSON);
  }

  @Test
  public void getJsonsNull() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils.getJsons(null));
  }

  @Test
  public void getObjectHappyDay() throws JsonProcessingException {
    final TestKlass testKlass = MappingUtils.getObject(ARTUR_JSON, TestKlass.class);
    assertThat(testKlass).isEqualTo(artur);
  }

  @Test
  public void getObjectEmptyString() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils.getObject("", TestKlass.class));
  }

  @Test
  public void getObjectNullKlass() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils.getObject(ARTUR_JSON, null));
  }

  @Test
  public void getObjecstHappyDay() throws JsonProcessingException {
    final List<TestKlass> objects = MappingUtils.getObjects(ARTUR_KAROLINA_JSON, TestKlass.class);
    assertThat(objects).contains(artur);
    assertThat(objects).contains(karolina);
  }

  @Test
  public void getObjectsEmptyString() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils.getObjects("", TestKlass.class));
  }

  @Test
  public void getObjectsNullKlass() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils.getObjects(ARTUR_JSON, null));
  }

  @Test
  public void getObjects2tHappyDay() throws JsonProcessingException {
    final List<TestKlass> objects = MappingUtils
        .getObjects(Lists.asList(ARTUR_JSON, KAROLINA_JSON, new String[0]), TestKlass.class);
    assertThat(objects).contains(artur);
    assertThat(objects).contains(karolina);
  }


  @Test
  public void getObjects2NullKlass() {
    Assertions.assertThrows(IllegalArgumentException.class,
        () -> MappingUtils
            .getObjects(Lists.asList(ARTUR_JSON, KAROLINA_JSON, new String[0]), null));
  }

  static class TestKlass {

    @JsonProperty
    private String firstName;
    @JsonProperty
    private String lastName;

    public TestKlass() {
    }

    public TestKlass(String firstName, String lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }

    public String getFirstName() {
      return firstName;
    }

    public String getLastName() {
      return lastName;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof TestKlass)) {
        return false;
      }
      TestKlass testKlass = (TestKlass) o;
      return Objects.equals(firstName, testKlass.firstName)
          && Objects.equals(lastName, testKlass.lastName);
    }

    @Override
    public int hashCode() {
      return Objects.hash(firstName, lastName);
    }
  }
}
